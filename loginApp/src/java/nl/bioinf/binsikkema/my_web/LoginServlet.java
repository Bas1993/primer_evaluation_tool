/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.binsikkema.my_web;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nl.bioinf.bnsikkema.my_web.scripts.User;

/**
 *
 * @author bas
 */
public class LoginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
           String user = request.getParameter("username");
            String pass = request.getParameter("password");
            
            HttpSession session = request.getSession();
            session.setMaxInactiveInterval(10);
            String sessionUser = (String) session.getAttribute("user");
            
            
            if(sessionUser == null) {
                User userObj = checkUserInput(user, pass);
            
                if(userObj == null) {
                    String errorMessage = "wrong stuff";
                    request.setAttribute("error", errorMessage);
                    RequestDispatcher view = request.getRequestDispatcher("login.jsp");
                    view.forward(request, response); 
                }
                else{
                    session.setAttribute("user", userObj);
                    RequestDispatcher view = request.getRequestDispatcher("contentJsp.jsp");
                    
                    view.forward(request, response); 
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("login.jsp");
        view.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
//            String user = request.getParameter("username");
//            String pass = request.getParameter("password");
//            
//            HttpSession session = request.getSession();
//            session.setMaxInactiveInterval(6);
//            String sessionUser = (String) session.getAttribute("user");
//            
//            
//            if(sessionUser == null) {
//                User userObj = checkUserInput(user, pass);
//            
//                if(userObj == null) {
//                    String errorMessage = "wrong stuff";
//                    request.setAttribute("error", errorMessage);
//                    RequestDispatcher view = request.getRequestDispatcher("login.jsp");
//                    view.forward(request, response); 
//                }
//                else{
//                    request.setAttribute("user", userObj);
//                    RequestDispatcher view = request.getRequestDispatcher("contentJsp.jsp");
//                    
//                    view.forward(request, response); 
//                }
//            }
          
    }
 

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>


   private User checkUserInput(String name, String pass) {
        if(name.equals("Bas") && pass.equals("Sikkema")) {
            User u = new User();
            u.setUsername(name);
                 
            return u;
        } else {
            return null;
        }
                
    }
}