<%-- 
    Document   : login
    Created on : 27-nov-2015, 14:26:03
    Author     : bas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>My super secret page</title>
    </head>
    <body>
        <h1>Content of corporate site</h1>
        <h3>${requestScope.errorMessage}</h3>
        <jsp:include page="includes/login_form.jsp" />
        
    </body>
</html>
