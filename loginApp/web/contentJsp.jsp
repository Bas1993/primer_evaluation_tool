<%-- 
    Document   : contentJsp
    Created on : 27-nov-2015, 16:15:53
    Author     : bas
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <base href = "${initParam.baseURL}">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:choose>
            <c:when test = "${sessionScope.user != null}">
                <h1>Hello ${sessionScope.user.name}</h1>   
                secret content
            </c:when>
            <c:otherwise>
                <h3>You are not logged in to our site! Please do so first </h3>
                <jsp:include page="includes/login_form.jsp" />
            </c:otherwise>
        </c:choose>
      

    </body>
</html>