/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.bnsikkema.servletmap;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nl.bioinf.bnsikkema.scriptmap.Author;
import nl.bioinf.bnsikkema.scriptmap.Books;
import nl.bioinf.bnsikkema.scriptmap.UserClass;

/**
 *
 * @author bas
 */

public class login_servlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
           String user = request.getParameter("username");
           String pass = request.getParameter("password");
           
           HttpSession session = request.getSession();
           session.setMaxInactiveInterval(5);

           UserClass sessionUser = (UserClass) session.getAttribute("user");
           if (sessionUser == null) {
               UserClass userObj =  checkUserInformation(user,pass);
               if (userObj == null) {
                   String errorMessage = "wrong username and/or password, please try again";
                   request.setAttribute("errorMessage", errorMessage);
                   RequestDispatcher view = request.getRequestDispatcher("Jsps/login.jsp");
                   view.forward(request, response);
               } else {
                  
                   session.setAttribute("user", userObj);
                   request.setAttribute("books", getBooks());
                   RequestDispatcher view = request.getRequestDispatcher("Jsps/content.jsp");
                   view.forward(request,response);
               
                   
               }
           } else {
               session.setAttribute("books", getBooks());
               RequestDispatcher view = request.getRequestDispatcher("Jsps/content.jsp");
               view.forward(request, response);
           }
           
           
            
        }
        
        
    }
    
    private UserClass checkUserInformation(final String user, final String pass) {
       if (user.equals("Bas") && pass.equals("Sikkema")) {
           UserClass u = new UserClass();
           u.setUsername(user);

           return u;
       }
       else if (user.equals("Henk") && pass.equals("Henkje")) {
           UserClass u = new UserClass();
           u.setUsername(user);

           
           return u;
       }
       else {
           return null;
       }
    }
    
    private List<Books> getBooks() {
        List<Books> books = new ArrayList();
        Author bas = new Author("Bas","Nick","Sikkema");
        Author henk = new Author("Henk",null,"bont");
        
//      /  Author Henk = new Author();
//        Henk.setName("Henk");
//        Henk.setLastName("leeg");
//        
//        Bas.setName("Bas");
//        Bas.setLastName("Sikkema");
//        Books book = new Books("how to make bombs", Bas, 666);
//        Books book2 = new Books("how to destroy the world", Henk, 6669);
//        
//        Bas.setBibliography(book);
//        Henk.setBibliography(book2);
        
//  
        books.add(new Books("how to destroy the world",bas, 666));
         books.add(new Books("How to build a bomb", henk,  65));
//         
//        books.add(book);
//        books.add(book2);
         
        
        return books;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("LoginForm.jsp");
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
