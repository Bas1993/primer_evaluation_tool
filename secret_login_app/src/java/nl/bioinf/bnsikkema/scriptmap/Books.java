/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.bnsikkema.scriptmap;

/**
 *
 * @author bas
 */
public class Books {
    private String title;
    private Author author;
    private int ISBN;

    
//    public void Author getAuthor() {
//        return Author;
//    }

   

    
    public Books(String title, Author author,int isbn) {
        setTitle(title);
        setAuthor(author);
        setISBN(isbn);
    }
    
     public void setAuthor(Author Author) {
        this.author = author;
    }
    public Author getAuthor() {
        return author;
    }
    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public int getISBN() {
        return ISBN;
    }

    public void setISBN(int ISBN) {
        this.ISBN = ISBN;
    }
    
    
    
//    
//    public String toString() {
//        return this.title + "|" + this.author +"|" + this.ISBN;
//    }
}
