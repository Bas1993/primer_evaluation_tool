/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.bnsikkema.scriptmap;

import java.util.List;

/**
 *
 * @author bnsikkema
 */
public final class Author {
    
   // public Author(){}
    public Author(String firstName, String between, String lastName){
        
        this.setFirstName(firstName);
        this.setLastName(lastName);
        init(between);
       
    }
    
   
    
    private String firstName;
    private String between;
    private String lastName;
    private List<Books> bibliography;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Books> getBibliography() {
        return bibliography;
    }

    public void setBibliography(List<Books> bibliography) {
        this.bibliography = bibliography;
    }

    public int numberOfbooks() {
        return this.bibliography.size();
    }
    
    
    public void init(String between) {
        if (between != null) {
            this.between = between;
        }
    }
    
    
   
    
    

 
    
    @Override
    public String toString() {
        return this.firstName + this.between + this.lastName;
    }
}
