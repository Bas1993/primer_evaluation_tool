<%-- 
    Document   : content
    Created on : 4-dec-2015, 12:11:05
    Author     : bas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <base href = "${initParam.baseURL}">
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:choose>
            <c:when test = "${sessionScope.user != null}">
                <c:choose>
                    <c:when test = "${sessionScope.user.name == 'Bas'}">
                        <c:redirect url="http://www.bioinf.nl/~bnsikkema"/>

                    </c:when>

                    <c:otherwise>
                        <h1>Hello ${sessionScope.user.name}, welcome to the secret page</h1>
                        <table border="2" style="width:100%">  
                            <caption>Books</caption>
                            <tr>
                                <th>title</th>
                                <th>Author</th>
                                <th>ISBN</th>
                            </tr>
                            <c:forEach var = "book" items= "${requestScope.books}">

                                <tr>
                                    <td>${book.getTitle()}</td>
                                    <td>${book.getAuthor()}</td>
                                    <td>${book.getISBN()}</td>
                                </tr>    


                            </c:forEach>   
                        </table>
                    </c:otherwise>
                </c:choose>

            </c:when>

            <c:otherwise>
                <h2>You are not logged in, please do so</h2>
                <jsp:include page="includes/login_form.jsp" />
            </c:otherwise>    
        </c:choose>    
    </body>
</html>
