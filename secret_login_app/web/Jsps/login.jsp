<%-- 
    Document   : login
    Created on : 4-dec-2015, 11:22:06
    Author     : bas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>${requestScope.errorMessage}</h1>
        <jsp:include page="../Includes/login_form.jsp" />
    </body>
</html>
