# Primer evaluation tool #
#Description#
The function of this application is to extract interesting and useful information from nucleotide primers given by the user. The front page of the application will contain a screen with two input boxes where the primers should be added. When the user clicks on the process button, the primers will be checked. If the primers are not valid (are not the same length, not minimal five bases long or have illegal characters) a error message will appear to inform the user about his mistake. If both primers are valid, the sequences will be used to calculate and determine some useful information: the CG percentage, the melting temperature, the maximum homo polymeric stretch and the maximum intermolecular identity. 
The table on the bottom of the page will contain this information plus the sequences of the primers.

###How to get started###
1. Clone the application to netbeans (```Bas_Sikkema_Primer_evaluation_tool```).
2. Run the application(```frontEnd.jsp```, ```index.jsp``` or the global folder (with the web symbol) they all work)
3. Try the application that has appeared in your web browser, use the guidelines in  the information container on the front page or in the description in this readMe. There is also a help button on the front page with some basic information about legal input.
 
### Used software ###
* apache tomcat 8.0.27**
* java 8
* javascript
* HTML5
* CSS3
* Netbeans 8.1

** The tomcat port that is used to test this application is 8084. I have changed this to 8080 in the used base url (```web.xml```) because otherwise the application won't work for users who use the 8080 port (I assume most users). Somehow I can't change my default port to 8080.

### Also included
A javadoc file is included with the application, you can find it in the application file ( ```Bas_Sikkema_Primer_evaluation_tool```).

### Additional information (important)  

Through a strange merge-situation with bitbucket and Netbeans, the repository of this application contains two additional files, namely: "```loginApp```" and "```Secret_login_app```". I have tried my best to get them out of my online repository but I could not succeed. The files have nothing to do with the application and do not affect it in any way. But it is sloppy anyhow, so I apologize for that. The application folder is called ```Bas_Sikkema_Primer_evaluation_tool```.