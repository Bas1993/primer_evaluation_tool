/*
 * PrimeriInformation.java
 * A utilityclass that calculates all sorts of primerinformation by its sequence through different methods
 *
 */
package nl.bioinf.bnsikkema.java;

import java.text.DecimalFormat;

/**
 *
 * @author Bas Sikkema
 */
public final class PrimerInformation {

    /**
     * private constructor.
     */
    private PrimerInformation() {
    }

    /**
     * Gets CG percentage of a primer.
     *
     * @param primer a user primer
     */
    public static void getCgPercentageAndMeltingTemp(final Primer primer) {
        String sequence = primer.getSequence();
        double cgNumber = 0.0;
        double primerLength = sequence.length();
        for (int i = 0; i < sequence.length(); i++) {
            /*only the four main nucleotide bases are excepted*/
            if (sequence.substring(i, i + 1).toUpperCase().equals("G")
                    || sequence.substring(i, i + 1).toUpperCase().equals("C")) {
                cgNumber += 1;
            }
        }
        /*percentages are rounded at one decimal*/
        DecimalFormat df = new DecimalFormat("#.#");
        /*cg percentage formula*/
        primer.setCgPercentage(df.format((cgNumber / primerLength) * 100));
        /*melting temperature formula*/
        primer.setMeltingTemp(df.format(4 * cgNumber + 2 * (primerLength - cgNumber)));
    }

    /**
     * This function determines the maximum3’-inter-and intra-molecular identity. .
     *
     * @param primer1 the first user primer
     * @param primer2 the second user primer
     */
    public static void getMaxmolecularIdentity(final Primer primer1, final Primer primer2) {
        String sequence1 = primer1.getSequence();
        String sequence2 = primer2.getSequence();
        StringBuilder seq2 = new StringBuilder().append(primer2.getSequence());
        seq2 = seq2.reverse();
        String nucs = "ATGC";
        String complements = "TACG";
        int counter = 0;
        for (int i = 0; i < sequence1.length(); i++) {
            if (nucs.indexOf(sequence1.charAt(i)) == complements.indexOf(seq2.toString().charAt(i))
                    && nucs.indexOf(sequence1.charAt(i)) != -1) {
                counter++;
            } else {
                break;
            }
        }
        primer1.setMaxMolIdentity(counter);
    }

    /**
     * Determines the maximum homopolymer stretch.
     *
     * @param primer a user primer
     */
    public static void getMaxHomopolymerStretch(final Primer primer) {
        String sequence = primer.getSequence().toLowerCase();
        int maxRepeats = 1;
        int repeats = 1;
        /*a repeat is always at least one base long*/
        int i = -1;
        while (i < sequence.length() - 2) {
            i++;
            if (sequence.substring(i, i + 1).equals(sequence.substring(i + 1, i + 2))) {
                /*linked bases are compared, when they are equal, a repeat is found*/
                repeats++;
                if (repeats >= maxRepeats) {
                    maxRepeats = repeats;
                }
            } else {
                /*if the bases are not equal the repeat counter is reset to 1*/
                if (repeats >= maxRepeats) {
                    maxRepeats = repeats;
                }
                repeats = 1;
            }
        }
        primer.setMaxHomopolymericStretch(maxRepeats);
    }

    /**
     * A function that crops the sequence of a primer when it is longer than 30 bases.
     *
     * @param primer a user primer
     * @param primerSeq the sequence of the user primer
     */
    public static void setSequence(final Primer primer, final String primerSeq) {
        int maxPrimerLength = 30;
        if (primerSeq.length() > maxPrimerLength) {
            String croppedSequence = primerSeq.substring(0, 30).toUpperCase() + "... +" + (primerSeq.length()
                    - maxPrimerLength + "bp");
            primer.setSequence(croppedSequence);
        } else {
            primer.setSequence(primerSeq);
        }
    }
}
