/*
 * Primer.java
 * A javabean that can contain all information of a given primer
 *
 */
package nl.bioinf.bnsikkema.java;

/**
 *
 * @author bas
 */
public class Primer {
    /**
     * number.
     * The number of the primer (1 or 2)
     */
    private int number;
    /**
     * sequence.
     * The sequence of the primer (may be cropped)
     */
    private String sequence;
    /**
     * meltingTemp.
     * The melting temperature of the primer
     */
    private String meltingTemp;
    /**
     * cgPercentage.
     * The CG percentage of the primer
     */
    private String cgPercentage;
    /**
     * maxMolIdentity.
     * The maximum molicular identity of the primer (with the other primer)
     */
    private int maxMolIdentity;
    /**
     * maxHomopolymericStretch.
     * The maximum number of repeats in the primersequence
     */
    private int maxHomopolymericStretch;

    /**
     * getNumber().
     * gets and returns primer number
     * @return number
     */
    public int getNumber() {
        return this.number;
    }

    /**
     * setnumber().
     * sets primer number
     * @param number primer number
     */
    public void setNumber(final int number) {
        this.number = number;
    }
    /**
     * getSequence().
     * gets and returns primer sequence
     * @return number
     */
    public String getSequence() {
        return sequence;
    }

    /**
     * setSequence().
     * sets primer sequence
     * @param sequence primer sequence
     */
    public void setSequence(final String sequence) {
        this.sequence = sequence;
    }

    /**
     * getMeltingTemp().
     * gets and returns melting temperature of the primer
     * @return meltingTemp
     */
    public String getMeltingTemp() {
        return meltingTemp;
    }

    /**
     * setMeltingTemp().
     * Sets melting temperature of the primer.
     * @param meltingTemp melting temperature
     */
    public void setMeltingTemp(final String meltingTemp) {
        this.meltingTemp = meltingTemp;
    }

    /**
     * getCgPercentage().
     * gets and returns cg percentage of the primer
     * @return cgPercentage
     */
    public String getCgPercentage() {
        return cgPercentage;
    }

    /**
     * setCgPercentage().
     * sets CG percentage
     * @param cgPercentage CG percentage
     */
    public void setCgPercentage(final String cgPercentage) {
        this.cgPercentage = cgPercentage;
    }

    /**
     * getMaxMolIdentity().
     * gets and returns maximum molecular identity
     * @return maxmolIdentity
     */
    public int getMaxMolIdentity() {
        return maxMolIdentity;
    }

    /**
     * setMaxMolIdentity().
     * sets maximum molecular identity
     * @param maxMolIdentity maximum molecular identity
     */
    public void setMaxMolIdentity(final int maxMolIdentity) {
        this.maxMolIdentity = maxMolIdentity;
    }

    /**
     * getMaxHomopolymericStretch().
     * gets and returns maximum homopolymeric stretch
     * @return maxHomopolymericStretch
     */
    public int getMaxHomopolymericStretch() {
        return maxHomopolymericStretch;
    }

    /**
     * setMaxHomopolymericStretch().
     * sets maximum homopolymeric stretch
     * @param maxHomopolymericStretch max repeats
     */
    public void setMaxHomopolymericStretch(final int maxHomopolymericStretch) {
        this.maxHomopolymericStretch = maxHomopolymericStretch;
    }
}
