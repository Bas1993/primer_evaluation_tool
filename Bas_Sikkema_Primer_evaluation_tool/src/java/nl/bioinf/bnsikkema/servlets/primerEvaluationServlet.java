/*
 * Project servlet
 */
package nl.bioinf.bnsikkema.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nl.bioinf.bnsikkema.java.Primer;
import nl.bioinf.bnsikkema.java.PrimerInformation;

/**
 *
 * @author Bas Sikkema
 */
public class primerEvaluationServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            String primer1 = request.getParameter("primer1");
            String primer2 = request.getParameter("primer2");
            HttpSession session = request.getSession();
            session.setMaxInactiveInterval(10);
            List<Primer> primers = createPrimers(primer1, primer2);
            request.setAttribute("primers", primers);
            RequestDispatcher view = request.getRequestDispatcher("Jsp/frontPage.jsp");
            view.forward(request, response);
        }
    }

    /**
     * creates primers.
     *
     * @param primer1 user primer1
     * @param primer2 user primer2
     * @return primers A list with both primerobjects provided with all primer information
     */
    private List<Primer> createPrimers(final String primer1, final String primer2) {
        /**
         * The two primers are created as primerobjects and their number and sequence are directly set*
         */
        Primer p1 = new Primer();
        p1.setNumber(1);
        p1.setSequence(primer1.toUpperCase());
        Primer p2 = new Primer();
        p2.setNumber(2);
        p2.setSequence(primer2.toUpperCase());

        /*The utility methods are called to get the primerinformation based on the primersequence*/
        PrimerInformation.getCgPercentageAndMeltingTemp(p1);
        PrimerInformation.getCgPercentageAndMeltingTemp(p2);
        PrimerInformation.getMaxmolecularIdentity(p1, p2);
        PrimerInformation.getMaxmolecularIdentity(p2, p1);
        PrimerInformation.getMaxHomopolymerStretch(p1);
        PrimerInformation.getMaxHomopolymerStretch(p2);
        PrimerInformation.setSequence(p1, primer1);
        PrimerInformation.setSequence(p2, primer2);
        List<Primer> primers = new ArrayList();
        /**
         * Primers are added to a list and returned *
         */
        primers.add(p1);
        primers.add(p2);
        return primers;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
