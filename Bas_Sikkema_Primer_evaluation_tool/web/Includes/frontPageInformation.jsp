<%-- 
    Document   : frontPageInformation
    Created on : 16-dec-2015, 16:55:56
    Author     : Bas Sikkema
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <base href="${initParam.baseURL}">
        <link href="Css/primerToolCss.css" rel="stylesheet" type="text/css"/>
        <script src="Js/lib/jquery.js" type="text/javascript"></script>
        <script src="Js/frontPage.js" type="text/javascript"></script>
        <script src="Js/checkPrimers.js" type="text/javascript"></script>
        <script src="Js/keepUpWithInput.js" type="text/javascript"></script>
        <title>JSP Page</title>
    </head>

    <body> 
        <div id="content" >
            <center><button id="information">hide information</button></center>            
            <p id ="informationText" style="display: 'block'"><b> 
                    This application will process your given primers and will get all the
                    useful basic information about these primers. The information contains 
                    the cg-percentage, the meltingtemperature, the maximum homopolymeric stretch
                    and the maximum inter molecular identity. Al these pieces of information will 
                    be summed up in the table at the bottom of this page.</b>
            </p> 
            <br>
            <div id="error" ><center></center></div>
            <div id = "primers">
                <center>    
                    <h2><b>please give two primers</b></h2>
                    <form name = "inputPrimers"  action="process" onsubmit="return checkPrimers()" method="post">
                        <tr><td><b> primer 1</b> </td><td><input type="text" name="primer1" onkeyup="return countInput(1)" onClick="return countInput(1)"id = "primer1" placeholder="primer 1" title="please insert a primer of at least 5 nucleotides" required> <b id="p1Len">0</b></tr>
                        <tr><td><b> primer 2</b> </td><td><input type="text" name="primer2" onkeyup="return countInput(2)" onClick="return countInput(2)"id = "primer2" placeholder= "primer 2" title="please insert a primer of at least 5 nucleotides" required> <b id="p2Len">0</b></tr>
                        <tr><td> <input type="submit" value = process ></td></tr>                
                    </form>

                    <button id="help">help</button>
                    <p id ="helpText" style="display: none"> 
                        The primers must be at least five nucleotides long and both need to be the same length.
                        Only the four main bases are excepted (A,T,G,C).<p>
                </center>
            </div> 
            <br>
            <div id="table">
                <table border="2" style="width:100%"> 
                    <caption><b>Primer information</b></caption>
                    <tr>
                        <th>Primer</th>
                        <th>sequence</th>
                        <th>CG percentage</th>    
                        <th>melting temperature (in K)</th>
                        <th>max homopolymer stretch</th>
                        <th>Maximum 3' inter molecular identity</th>
                    </tr>
                    <c:forEach var = "primer" items="${requestScope.primers}">
                        <tr>                
                            <td>Primer ${primer.getNumber()} </td>
                            <td>${primer.getSequence()}</td>
                            <td>${primer.getCgPercentage()}</td>
                            <td>${primer.getMeltingTemp()}</td>
                            <td>${primer.getMaxHomopolymericStretch()}</td>
                            <td>${primer.getMaxMolIdentity()}</td>
                        </tr>
                    </c:forEach>
                </table>


            </div>
            <br>
            <center><form action="Jsp/frontPage.jsp#table">
                    <input type="submit" value="Clear table">
                </form></center>
        </div>


    </body>
</html>
