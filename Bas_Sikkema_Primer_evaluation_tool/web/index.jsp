<%-- 
    Document   : frontPage
    Created on : 24-dec-2015, 11:59:27
    Author     : bas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <base href="${initParam.baseURL}">
        <link href="Css/primerToolCss.css" rel="stylesheet" type="text/css"/>
        <script src="Js/lib/jquery.js" type="text/javascript"></script>
        <script src="Js/frontPage.js" type="text/javascript"></script>
        <title>JSP Page</title>
    </head>
    <body>
        <h1><center>primer information application</center></h1>
        
       <jsp:include page="/Includes/frontPageInformation.jsp"/>
        <jsp:include page="/Includes/footer.jsp"/>
    </body>
</html>
