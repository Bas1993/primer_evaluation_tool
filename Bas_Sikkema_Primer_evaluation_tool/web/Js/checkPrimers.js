/* 
 * Js file that checks if input is correct
 */

function checkPrimers() {

    var primer1 = document.getElementById("primer1").value;
    var primer2 = document.getElementById("primer2").value;
    
    if (!checkInputPrimer(primer1) || !checkInputPrimer(primer2)) {
        /**Set error message**/
        var errorMessage = createErrorMessage(1);
        document.getElementById("error").innerHTML = errorMessage;
        document.getElementById("error").style.display = "block";
        return false;
        /**Checks primer lengths**/
    } else if (primer1.length !== primer2.length) {
        /**Set error message**/
        var errorMessage = createErrorMessage(2);
        document.getElementById("error").innerHTML = errorMessage;
        document.getElementById("error").style.display = "block";
        return false;
    } else {
        document.getElementById("table").style.display = "block";
    }
}

/**
 * Checks type of error 
 * @param {type} primer
 * @returns {Boolean}
 */
function checkInputPrimer(primer) {
    var check = true;
    if (primer.length < 5) {
        check = false;
    } else {
        var acceptedNucs = "ATCG";
        for (i = 0; i < primer.length; i++) {
            if (!acceptedNucs.contains(primer.substring(i, i + 1).toUpperCase())) {
                check = false;
            }
        }
    }
    return check;
}

/**
 * Generate errorText
 * @param {type} errorType
 * @returns {String} 
 */
function createErrorMessage(errorType) {
    var message;
    if (errorType === 1) {
        message = "Something went wrong,"
                + " please check if your primers are at least 5 bases long and contain only nucleotides.";
    } else {
        message = "Something went wrong, primers are not the same length";
    }
    return message;

}

/**
 * Hides errormessage onload
 * @returns {undefined}
 */
function hideError() {
    document.getElementById("error").style.display = "none";
}


$(document).ready(hideError);

