/* 
 * Js file that makes frontpage dynamic
 */


/**
 * toggle visibility of the help on click
 */
$(document).ready(function(){
    var check;
    $("#help").click(function(){
        if (check !== true) {
        $("#helpText").show();
        check = true;
    }
    else {
        $("#helpText").hide();
        check = false;
    }
    });
});

/**
 * toggle visibility of the information on click
 */  
$(document).ready(function(){
    var check;
    $("#information").click(function(){
        if (check === true) {
            
        $("#informationText").show();
        document.getElementById("information").innerHTML = "hide information";
        check = false;
    }
    else {
        $("#informationText").hide();
        document.getElementById("information").innerHTML = "show information";
        check = true;
    }
    });
});